from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
import time
import re
import mysql.connector
from bs4 import BeautifulSoup
import requests
import json
import click
from datetime import datetime
import argparse
import logging

# Creation log file

from logging.handlers import RotatingFileHandler


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
file_handler = RotatingFileHandler('who_scored_log_file.log', 'a', 1000000, 1)
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

#stream_handler = logging.StreamHandler()
#stream_handler.setLevel(logging.DEBUG)
#logger.addHandler(stream_handler)


""" Defining variables in order to execute the script from CLI"""

# parser = argparse.ArgumentParser()
# parser.add_argument("num_page")
# parser.add_argument("user")
# parser.add_argument("password")
# parser.add_argument("auth_plugin")
#
#
# args = parser.parse_args()
# num_page = args.num_page
# user = args.user
# password = args.password
# auth_plugin = args.auth_plugin


PAGE_TO_SCRAP = {'Premier League': 'https://www.whoscored.com/Regions/252/Tournaments/2/Seasons/7361/Stages/16368/'
                                   'PlayerStatistics/England-Premier-League-2018-2019',
                 'Serie A': 'https://www.whoscored.com/Regions/108/Tournaments/5/Seasons/7468/Stages/16548/'
                            'PlayerStatistics/Italy-Serie-A-2018-2019',
                 'Liga': 'https://www.whoscored.com/Regions/206/Tournaments/4/Seasons/7466/Stages/16546/'
                         'PlayerStatistics/Spain-La-Liga-2018-2019',
                 'Ligue 1': 'https://www.whoscored.com/Regions/74/Tournaments/22/Seasons/7344/Stages/16348/'
                            'PlayerStatistics/France-Ligue-1-2018-2019',
                 'Bundesliga': 'https://www.whoscored.com/Regions/81/Tournaments/3/Seasons/7405/Stages/16427/'
                               'PlayerStatistics/Germany-Bundesliga-2018-2019',
                 'Champions League': 'https://www.whoscored.com/Regions/250/Tournaments/12/Seasons/7352/Sta'
                                     'ges/16704/PlayerStatistics/Europe-UEFA-Champions-League-2018-2019',
                 'Europa League': 'https://www.whoscored.com/Regions/250/Tournaments/30/Seasons/7353/Stage'
                                  's/16705/PlayerStatistics/Europe-UEFA-Europa-League-2018-2019'}


URL = [PAGE_TO_SCRAP['Premier League'], PAGE_TO_SCRAP['Serie A'], PAGE_TO_SCRAP['Liga'],
       PAGE_TO_SCRAP['Ligue 1'], PAGE_TO_SCRAP['Bundesliga']]

TIME_BETWEEN_PAGES_LOADING = 5
X_PATH_LINES = ['//*[@id="player-table-statistics-body"]/tr[', ']/td[']
X_PATH_ALL_PLAYERS = '//*[@id="apps"]/dd[2]/a'
X_PATH_HEADER = '//*[@id="player-table-statistics-head"]'
ID_HEADER = 'player-table-statistics-head'
X_PATH_TABS = {'Summary': '//*[@id="stage-top-player-stats-options"]/li[1]/a',
               "Defensive": '//*[@id="stage-top-player-stats-options"]/li[2]/a',
               'Offensive': '//*[@id="stage-top-player-stats-options"]/li[3]/a',
               'Passing': '//*[@id="stage-top-player-stats-options"]/li[4]/a'}
X_PATH_TABS_2 = [X_PATH_TABS['Summary'], X_PATH_TABS['Defensive'], X_PATH_TABS['Offensive'], X_PATH_TABS['Passing']]
ID_NEXT_BUTTON = '//*[@id="next"]'
IDs_PER_PAGES = ['statistics-table-summary', 'statistics-table-defensive',
                 'statistics-table-offensive', 'statistics-table-passing']
TABLE_ID = ['stage-top-player-stats-summary', 'stage-top-player-stats-defensive',
            'stage-top-player-stats-offensive', 'stage-top-player-stats-passing']

# num_page, user, password, auth_plugin = '3', 'root', 'AR267IEbonan', 'mysql_native_password'
num_page, user, password, auth_plugin = '50', 'root2', 'Leomessi9', 'mysql_native_password'


def extract_table(table_id, browser, num_page):
    """ Extract table from the website
    """
    page_number = 1
    player_number = 1
    my_players = []
    players_name = []
    my_table = browser.find_element_by_id(table_id)
    time.sleep(TIME_BETWEEN_PAGES_LOADING)
    header_row = my_table.find_element_by_id(ID_HEADER).text.split(" ")[1:]
    header_row.insert(1, 'Clubs')
    header_row.insert(2, 'Age')
    header_row.insert(3, 'Position')
    # while the next button can be pressed
    while True:
        print("Page number {}".format(page_number))
        for tr in range(1, 11):
            player = []
            try:
                info_player = my_table.find_element_by_xpath(X_PATH_LINES[0]+str(tr)+']/td[3]').text
            except NoSuchElementException:
                return header_row, my_players
            info_player = re.split(',|\n', info_player)
            for i in range(0, 4):
                player.append(info_player[i])

            for td in range(4, 15):
                player.append(my_table.find_element_by_xpath(X_PATH_LINES[0]+str(tr)+X_PATH_LINES[1]+str(td)+']').text)
            print("Players number {}, name: {}".format(player_number, player[0]))
            logger.info(player[0])
            my_players.append(player)
            if player[0] not in players_name: # A Corriger
                players_name.append(player[0])
            else:
                return header_row, my_players
            player_number += 1
        if page_number == num_page:
            return header_row, my_players
        page_number += 1
        try:
            # Try to press next button
            my_table.find_element_by_xpath(ID_NEXT_BUTTON).click()
        except NoSuchElementException:
            break
        time.sleep(TIME_BETWEEN_PAGES_LOADING)


def list_of_dictionaries_creation(header, players_list):
    """ Create a dictionary for every player, using the header row created in extract table
    """
    final_dictionaries_list = []
    for player_data in players_list:
        my_dict = {}
        for i in range(0, len(player_data)):
            my_dict[header[i]] = player_data[i]
        final_dictionaries_list.append(my_dict)
    return final_dictionaries_list


def to_sql_league(league, user, password, auth_plugin):
    """ Create a table with every league
    """
    cnx = mysql.connector.connect(user=user, password=password, auth_plugin=auth_plugin)
    my_cursor = cnx.cursor()
    try:
        my_cursor.execute("CREATE DATABASE who_scored")
    except mysql.connector.errors.DatabaseError:
        print("DB already exists")
    my_cursor.execute("USE who_scored")
    try:
        script = "CREATE TABLE Leagues (League_ID INT(10) AUTO_INCREMENT PRIMARY KEY, League VARCHAR(100) UNIQUE)"
        my_cursor.execute(script)
    except mysql.connector.errors.ProgrammingError:
        print("Table already exists")
    try:
        sql = "INSERT INTO Leagues (League) VALUES ('{}');".format(league)
        my_cursor.execute(sql)
    except mysql.connector.IntegrityError:
        pass
    cnx.commit()


def to_sql_clubs(final_scrap, league, user, password, auth_plugin):
    """ Create a table with every clubs, with league as foreign key
    """
    cnx = mysql.connector.connect(user=user, password=password, auth_plugin=auth_plugin)
    my_cursor = cnx.cursor()
    try:
        my_cursor.execute("CREATE DATABASE who_scored")
    except mysql.connector.errors.DatabaseError:
        print("DB already exists")
    my_cursor.execute("USE who_scored")
    try:
        script = "CREATE TABLE Clubs (League_ID INT(10), Club_id INT(10) AUTO_INCREMENT PRIMARY KEY, Club " \
                 "VARCHAR(100) UNIQUE, FOREIGN KEY (League_ID) REFERENCES Leagues(League_ID))"
        my_cursor.execute(script)
    except mysql.connector.errors.ProgrammingError:
        print("Table already exists")
    clubs = []
    for player in final_scrap:
        clubs.append(player['Clubs'])
    clubs = list(set(clubs))

    for club in clubs:
        try:
            sql = "INSERT INTO Clubs (League_ID, Club) SELECT League_ID, '{}' " \
                  "FROM Leagues WHERE League = '{}' LIMIT 1;".format(club, league)
            my_cursor.execute(sql)
        except mysql.connector.IntegrityError:
            pass
    cnx.commit()


def to_sql_players(final_scrap, user, password, auth_plugin):
    """ Create a table with every player, with clubs as foreign key
    """
    cnx = mysql.connector.connect(user=user, password=password, auth_plugin=auth_plugin)
    my_cursor = cnx.cursor()
    my_cursor.execute("USE who_scored")
    try:
        script = "CREATE TABLE Players (Club_id Int(6), Player_ID INT(6) AUTO_INCREMENT PRIMARY KEY ," \
                 "Player VARCHAR(100) UNIQUE, Age INT(6), Position VARCHAR(100), " \
                 "FOREIGN KEY (Club_id) REFERENCES Clubs(Club_id))"
        my_cursor.execute(script)
    except mysql.connector.errors.ProgrammingError:
        print("Table already exists")

    for element in final_scrap:
        player = element['Player']
        club = element['Clubs']
        age = int(element['Age'])
        position = element['Position']
        try:
            sql = 'INSERT INTO Players (Club_ID, Player, Age, Position) SELECT club_id, "{}", {}, "{}" ' \
                  'FROM Clubs WHERE club = "{}" LIMIT 1'.format(player, age, position, club)
            my_cursor.execute(sql)
        except mysql.connector.errors.IntegrityError:
            pass
    cnx.commit()


def to_sql_players_info(league, final_scrap, table_name, user, password, auth_plugin):
    """ Create a table with every player info, using clubs and player as foreign key
    """
    cnx = mysql.connector.connect(user=user, password=password, auth_plugin=auth_plugin)
    my_cursor = cnx.cursor()
    my_cursor.execute("USE who_scored")
    try:
        script = "CREATE TABLE Players_{} (OBS_ID INT(6) AUTO_INCREMENT PRIMARY KEY , Club_id Int(6), " \
                 "Player_ID INT(6), League_ID INT(6), Added_date DATETIME, " \
                 "Apps INT(6), Min_played INT(6), Goals INT(6), Assists INT(6), Yel INT(6), Red INT(6), " \
                 "SpG FLOAT(8, 2), PS_percent FLOAT(8, 2), AerialsWon FLOAT(8, 2), MotM INT(6), Rating FLOAT(8, 2), " \
                 "FOREIGN KEY (Club_id) REFERENCES Clubs(Club_id), " \
                 "FOREIGN KEY (Player_ID) REFERENCES Players(Player_ID)," \
                 "FOREIGN KEY (League_ID) REFERENCES Leagues(League_ID))".format(table_name)
        my_cursor.execute(script)
    except mysql.connector.errors.ProgrammingError:
        print("Table already exists")
    for element in final_scrap:
        apps = ''
        for key, value in element.items():
            if element[key] == '-':
                element[key] = 0
        for key, value in element.items():
            if key == 'Apps':
                i = 0
                try:
                    while element[key][i].isdigit() is True:
                        apps += element[key][i]
                        i += 1
                    element[key] = apps
                except IndexError:
                    break
        date = datetime.today().strftime('%Y-%m-%d %H:%M:%S.%f')
        try:
            sql = 'INSERT INTO Players_Summary ' \
                  '(League_ID, Club_ID, Player_ID, Added_date, Apps, Min_played, Goals, Assists, Yel, ' \
                  'Red, SpG, PS_percent, AerialsWon, MotM, Rating ) ' \
                  'SELECT (SELECT League_ID FROM Leagues WHERE League = "{}"), club_id, ' \
                  '(SELECT Player_ID FROM Players ' \
                  'WHERE Player = "{}"),"{}", {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {} ' \
                  'FROM Clubs WHERE club = "{}" LIMIT 1'\
                .format(league, element['Player'], date, element['Apps'], element['Mins'],
                        element['Goals'], element['Assists'],
                        element['Yel'], element['Red'], element['SpG'], element['PS%'], element['AerialsWon'],
                        element['MotM'], element['Rating'], element['Clubs'])
            my_cursor.execute(sql)
        except mysql.connector.IntegrityError:
            pass
    cnx.commit()


# def main(num_page, user, password, auth_plugin):
def main():
    # i = 1
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    browser = webdriver.Chrome(chrome_options=options)
    # browser = webdriver.PhantomJS()
    for key, value in PAGE_TO_SCRAP.items():
        # Page opening
        browser.get(value)
        browser.execute_script("window.scrollTo(0, 540)")
        time.sleep(TIME_BETWEEN_PAGES_LOADING)
        table_id = TABLE_ID[0]
        extraction = extract_table(table_id, browser, int(num_page))
        final_scrap = list_of_dictionaries_creation(extraction[0], extraction[1])
        to_sql_league(key, user, password, auth_plugin)
        to_sql_clubs(final_scrap, key, user, password, auth_plugin)
        to_sql_players(final_scrap, user, password, auth_plugin)
        to_sql_players_info(key, final_scrap, table_id.split("-")[-1], user, password, auth_plugin)
        time.sleep(15)


# def main():
#     return(f(num_page, user, password, auth_plugin))


if __name__ == "__main__":
    main()


