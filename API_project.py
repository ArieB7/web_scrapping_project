#### Execution from CLI :  python path_to_python_script script.py

import mysql.connector
import requests
import pandas as pd

""" Here all the datas regarding soccer game from the API"""

# Notice the indexes of different pages containing our datas

# Premier League
#first_link = 441425
#last_link = 441803

# Bundesliga
first_link = 527315
last_link = 527620

# Ligue 1
first_link = 527819
last_link = 528198

# Liga
first_link = 528199
last_link = 528580

# Serie A
first_link = 528603
last_link = 528982

def scrap_API(index1, index2):
   """ Function which scrapped datas soccer games from API using indexes of each game"""
   l_strEvent = []
   l_idLeague = []
   l_dateEvent = []
   l_strHomeGoalDetails = []
   l_strAwayGoalDetails = []
   l_intHomeShots = []
   l_intAwayShots = []
   l_strHomeTeam = []
   l_strAwayTeam = []
   l_score_home = []
   l_score_away = []

   i = index1
   l = []
   while i < index2:
      d={}
      url = 'https://www.thesportsdb.com/api/v1/json/1/lookupevent.php?id=' + str(i)

      headers = {'X-Auth-Token': '605745d711254e77b3db095b98f1455e'}
      page = requests.get(url, "competitions?plan=TIER_ONE", headers=headers)

      info = page.json()
      info = info['events'][0]

      l_strEvent.append(info['strEvent'])
      l_idLeague.append(info['idLeague'])
      l_dateEvent.append(info['dateEvent'])
      l_strHomeGoalDetails.append(info['strHomeGoalDetails'])
      l_strAwayGoalDetails.append(info['strAwayGoalDetails'])
      l_intHomeShots.append(info['intHomeShots'])
      l_intAwayShots.append(info['intAwayShots'])
      l_strHomeTeam.append(info['strHomeTeam'])
      l_strAwayTeam.append(info['strAwayTeam'])
      l_score_home.append(info['intHomeScore'])
      l_score_away.append(info['intAwayScore'])

      d['Event'] = info['strEvent']
      d['Date'] = info['dateEvent']
      d['HomeTeam'] = info['strHomeTeam']
      d['AwayTeam'] = info['strAwayTeam']
      d['HomeScore'] = info['intHomeScore']
      d['AwayScore'] = info['intAwayScore']
      d['ScorersHome'] = info['strHomeGoalDetails']
      d['ScorersAway'] = info['strAwayGoalDetails']
      d['HomeShots'] =  info['intHomeShots']
      d['AwayShots'] = info['intAwayShots']

      l.append(d)

      i += 1


   return(l)


""" Creating lists which contain the datas we scrapped """

### Spanish League ###

Liga = scrap_API(528199,528580)

### French Ligue ###

French = scrap_API(527819, 528198)

### Premier League ###

#British = scrap_API(441425, 441803)

### German League ###

German = scrap_API(527315, 527620)

### Italian League ###

Italia = scrap_API(528603, 528982)


""" Mapping between API teams and Scrap teams"""

my_teams = {
"Bordeaux":"Bordeaux",
"Rennes": "Rennes",
"Toulouse": "Toulouse",
"Saint-Etienne": "St Etienne",
"Nimes": "Nimes",
"Lyon": "Lyon",
"Monaco": "Monaco",
"Montpellier": "Montpellier",
"Paris Saint-Germain": "Paris SG",
"Marseille": "Marseille",
"Strasbourg": "Strasbourg",
"Nantes": "Nantes",
"Guingamp": "Guingamp",
"Lille": "Lille",
"Amiens": "Amiens",
"Nice": "Nice",
"Dijon": "Dijon",
"Caen": "Caen",
"Reims": "Reims",
"Angers": "Angers",
"AC Milan": "Milan",
"Bologna": "Bologna",
"SPAL 2013": "SPAL 2013",
"Juventus": "Juventus",
"Chievo": "Chievo",
"Empoli": "Empoli",
"Atalanta": "Atalanta",
"Parma Calcio 1913": "Parme",
"Sampdoria": "Sampdoria",
"Inter": "Inter",
"Udinese": "Udinese",
"Roma": "Roma",
"Torino": "Torino",
"Sassuolo": "Sassuolo",
"Lazio": "Lazio",
"Napoli": "Napoli",
"Frosinone": "Frosinone",
"Cagliari": "Cagliari",
"Genoa": "Genoa",
"Fiorentina": "Fiorentina",
"Atletico Madrid": "Ath Madrid",
"Espanyol": "Espanol",
"Girona": "Girona",
"Leganes": "Leganes",
"Real Madrid": "Real Madrid",
"Eibar": "Eibar",
"Getafe": "Getafe",
"Barcelona": "Barcelona",
"SD Huesca": "Huesca",
"Villarreal": "Villarreal",
"Athletic Bilbao": "Ath Bilbao",
"Rayo Vallecano":"Rayo Vallecano",
"Celta Vigo": "Celta Vigo",
"Levante": "Levante",
"Real Betis": "Betis",
"Real Sociedad": "Sociedad",
"Sevilla": "Sevilla",
"Real Valladolid": "",
"Valencia": "Valencia",
"Deportivo Alaves": "Alaves",
"Borussia M.Gladbach": "M'gladbach",
"Mainz 05": "Mainz",
"Bayer Leverkusen": "Leverkusen",
"Eintracht Frankfurt": "Ein Frankfurt",
"VfB Stuttgart": "Stuttgart",
"Freiburg": "Freiburg",
"Fortuna Duesseldorf": "Fortuna",
"Hoffenheim": "Hoffenheim",
"Nuernberg": "Nurenberg",
"Augsburg": "Augsburg",
"Werder Bremen": "Werder Bremen",
"Borussia Dortmund": "Dortmund",
"Bayern Munich": "Bayern Munich",
"Hannover 96": "Hannover",
"Hertha Berlin": "Hertha",
"Schalke 04": "Schalke 04",
"Wolfsburg": "Wolfsburg",
"RasenBallsport Leipzig": "RasenBallsport Leipzig"
}
final = {}
for k, v in my_teams.items():
    final[v] = k




###      MYSQL         ###



username = 'root'

""" Second Step : Connecting to the DB"""



cnx = mysql.connector.connect(user=username, password='Leomessi9', database='who_scored')
cursor = cnx.cursor()

# Creating tables

league_list = [Liga, Italia, French, German]
league_name = ['Liga', 'Serie1', 'Ligue1', 'Bundesliga']

#league_list = [Liga]
#league_name = ['Liga_bis']

d = {}
d['Matchs'] = Liga + Italia + French + German



for key, value in d.items():
   cursor.execute("""CREATE TABLE IF NOT EXISTS """ + str(key) + """(Event varchar(100), Date varchar(50), HomeTeam INT, AwayTeam varchar(50), HomeScore varchar(10), AwayScore varchar(10), HomeShots varchar(10), AwayShots varchar(10))""")

   for data in value:
      if data['HomeTeam'] in final.keys():
         if data['AwayTeam'] in final.keys():
            print(data['HomeTeam'], (data['AwayTeam']))

            try:
               id_home = cursor.execute("Select Club_id from Clubs where club = '{}' ".format(final[data['HomeTeam']]))
               id_home = cursor.fetchone()[0]

               id_away = cursor.execute("Select Club_id from Clubs where club = '{}' ".format(final[data['AwayTeam']]))
               id_away = cursor.fetchone()[0]

               print(id_away)

               cursor.execute("""INSERT INTO """ + str(key) + """(Event, Date, HomeTeam, AwayTeam, HomeScore, AwayScore, HomeShots, AwayShots) VALUES (%s, %s, %s, %s,%s, %s,%s, %s)""",
                              (data['Event'], data['Date'], id_home,
                               id_away, data['HomeScore'], data['AwayScore'],
                              data['HomeShots'], data['AwayShots']))

               cnx.commit()
            except TypeError:
               pass



